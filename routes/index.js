var express = require('express');
var router = express.Router();


/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', {
        title: 'Home',
        name: '',
        latitude: -27.477,
        longitude: 131.036,
    });
});


module.exports = router;
