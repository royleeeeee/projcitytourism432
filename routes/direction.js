var express = require('express');
var router = express.Router();

// Pass the coordinates of the origin and destination to
// GoogleMap direction api
router.get('/:latH/:lngH/:latA/:lngA/:mode', function(req, res) {
    res.render('direction', {
        title: 'Directions',
        name: '',
        latH: req.params.latH,
        lngH: req.params.lngH,
        latA: req.params.latA,
        lngA: req.params.lngA,
        mode: req.params.mode,
    });
});

module.exports = router;
