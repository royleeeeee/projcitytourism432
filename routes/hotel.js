var express = require('express');
var router = express.Router();
var request = require("request");
var crypto = require('crypto');

// Credentials used to access EAN's API
const cidEAN = '504319';
const apiKeyEAN = '16r9b4309da10i8e9j301skit7';
const secretEAN = '7rb6vdi5u3ghr';

router.get('/:state/:city/:lat/:lng/:res', function(req, res) {

    // Creating a md5 hash signature required to request data from EAN's API
    var date = new Date();
    var unixTime = Math.round(date.getTime() / 1000);
    var sigEAN =  crypto.createHash('md5').update(apiKeyEAN + secretEAN + unixTime).digest('hex');

    // Constructing EAN's API URL
    var service = 'https://book.api.ean.com/ean-services/rs/hotel/';
    var version = 'v3/';
    var method = 'list/';
    var cid = '?cid=' + cidEAN;
    var rev = '&minorRev=99';
    var api = '&apiKey=' +apiKeyEAN;
    var lang = '&locale=en_US';
    var currency = '&currencyCode=AUD';
    var sig = '&sig=' + sigEAN;
    var requestType = '&xml=%3CHotelListRequest%3E%0A';
    var city = '%3Ccity%3E' +req.params.city + '%3C%2Fcity%3E%0A';
    var state = '%3CstateProvinceCode%3E' + req.params.state + '%3C%2FstateProvinceCode%3E%0A';
    var country = '%3CcountryCode%3EAU%3C%2FcountryCode%3E%0A';
    var arrival = '%3CarrivalDate%3E10%2F16%2F2017%3C%2FarrivalDate%3E%0A';
    var departure = '%3CdepartureDate%3E10%2F18%2F2017%3C%2FdepartureDate%3E%0A';
    var otherElement = '%3CRoomGroup%3E%0A%3CRoom%3E%0A';
    var numAdults = '%3CnumberOfAdults%3E2%3C%2FnumberOfAdults%3E%0A%';
    var otherElement2 = '3C%2FRoom%3E%0A%3C%2FRoomGroup%3E%0A';
    var resultAmount = '%3CnumberOfResults%3E' + req.params.res + '%3C%2FnumberOfResults%3E%0A';
    var otherElement3 = '%3C%2FHotelListRequest%3E';
    var resultType = '&_type=json';

    var urlEAN = service + version + method + cid + rev + api + lang + currency + sig + requestType + 
        city + state + country + arrival + departure + otherElement + numAdults + otherElement2 +
        resultAmount + otherElement3 + resultType;

    request({url: urlEAN, json: true}, function(error, response, body) {
        if (!error && res.statusCode === 200) {
            res.render('hotel', {
                title: 'Hotel -',
                state: req.params.state,
                name: req.params.city,
                city: req.params.city,
                latitude: req.params.lat,
                longitude: req.params.lng,
                num: req.params.res,
                url: urlEAN,
                json: body,
            });
        } else {
            res.status(response.statusCode).render('error');
        }
    })
});

module.exports = router;

