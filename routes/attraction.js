var express = require('express');
var router = express.Router();
var request = require("request");

// Credentials for GoogleMap's API
const apiKeyGoogle = "AIzaSyCfS03anpZOgmay_j_BwdBGmDWms5k4Naw";

// Prints out the first 20 attraction near the chosen hotel
router.get('/:type/:hotel/:lat/:lng/:rad', function(req, res) {
    var url = encodeURI("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" +
        req.params.lat + "," + req.params.lng + "&radius=" + req.params.rad +
        "&type=" + req.params.type + "&key=" + apiKeyGoogle);

    request({url: url, json: true}, function(error, response, body) {
        if (!error && res.statusCode === 200) {
            res.render('attraction', {
                title: 'Restaurants -',
                name: req.params.type,
                hotel: req.params.hotel,
                latitude: req.params.lat,
                longitude: req.params.lng,
                type: req.params.type,
                radius: req.params.rad,
                url: url,
                json: body,
            });
        } else {
            res.status(response.statusCode).render('error');
        }
    })
});

// Using the provided 'next_page_token' from Google Map's API, render
// the next 20 results
router.get('/:type/:hotel/:lat/:lng/:rad/:next', function(req,res) {
    var url = encodeURI("https://maps.googleapis.com/maps/api/place/nearbysearch/json?pagetoken=" +
        req.params.next + "&key=" + apiKeyGoogle)

    request({url: url, json: true}, function(error, response, body) {
        if (!error && res.statusCode === 200) {
            res.render('attraction', {
                title: 'Restaurants -',
                name: req.params.type,
                hotel: req.params.hotel,
                latitude: req.params.lat,
                longitude: req.params.lng,
                type: req.params.type,
                radius: req.params.rad,
                url: url,
                json: body,
            });
        } else {
            res.status(response.statusCode).render('error');
        }
    })

});

module.exports = router;

