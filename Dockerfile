FROM node:boron

COPY . /src

WORKDIR /src

COPY package.json .

RUN npm install

EXPOSE 3000

CMD ["npm", "start"]